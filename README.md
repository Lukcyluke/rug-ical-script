# RUG ical Script

These scripts pull the given subscribed calendars and change the details for the events on a very basic level.
With `sed`.

## Usage

Modify the `urls.txt` and the paths in `ical_script.sh` and run the script.
When the script is used on the X computers and the created `.ical` files are placed in `/home/wwwu/<username>/public/<subfolder>` they can be used as subcribed calendar in any program that supports these feature.
Just add a subcribed calendar with the url `http://user.informatik.uni-bremen.de/<username>/<subfolder>/<filename.ics>`.

## cron setup

To schedule the script to update the calendar `cron` can be used.
Run
``` bash
crontab -e
```
In the file insert some line like
```
30 15 * * * <path to .sh script>

```
This would run the script each day at 15:30.
The positions stand for
```
minute hour day-of-month month day-of-week command
```
0 is Sunday
