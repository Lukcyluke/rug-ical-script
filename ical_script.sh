# This script downloads my course schedule from the RUG website and adds locations and other stuff and published the ics files on my domain
# to get the occuring locations in the icals use
# grep LOCATION: downloads/* | sort | uniq -c | sort
# the workflow is: duplicate original event in fantsatical and edd geo location from https://www.rug.nl/staff/location/
# then copy the X-APPLE-STRUCTURED-LOCATION and LOCATION values into the sed command in replace_locations.sed and escape dots and backslashes and spaces

# to change the titles use replace_summary.se

cd /home/tiefens1/ical_script/
rm -r downloads/
mkdir -p downloads
cd downloads
echo "downloading"
wget -q -i ../urls.txt
cd ..

echo "replacing locations"
sed -i -f replace_location.sed downloads/*

echo "replacing course titles"
sed -i -f replace_summary.sed downloads/*

echo "adding course urls"
sed -i 's!BEGIN:VEVENT!BEGIN:VEVENT\nURL;VALUE=URI:url-to-course-site.com!g' downloads/KIM.SCHR03 # Handwriting recognition
sed -i 's!BEGIN:VEVENT!BEGIN:VEVENT\nURL;VALUE=URI:url-to-course-site.com!g' downloads/WBCS14001 # Ethical and Professional Issues
sed -i 's!BEGIN:VEVENT!BEGIN:VEVENT\nURL;VALUE=URI:url-to-course-site.com!g' downloads/WMCS16003 # Scalable Computing
sed -i 's!BEGIN:VEVENT!BEGIN:VEVENT\nURL;VALUE=URI:url-to-course-site.com!g' downloads/WMEE16001 # Global Change
sed -i 's!BEGIN:VEVENT!BEGIN:VEVENT\nURL;VALUE=URI:url-to-course-site.com!g' downloads/WMEE18001 # Climate Modelling
sed -i 's!BEGIN:VEVENT!BEGIN:VEVENT\nURL;VALUE=URI:url-to-course-site.com!g' downloads/WMEE18002 # Energy and complexity Nexus
echo "publish"
cp downloads/* ~/webspace/rug/
